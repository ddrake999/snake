    var canvas = document.getElementById("main");
    canvas.width = "600";
    canvas.height = "400";
    canvas.background = "#000";

    var snake = {
      "direction": "r",
      "body": [[3,0],[2,0],[1,0],[0,0]],
      "len": 4
    };

    var info = {
      "space": 20,
      "size": 17
    };

    var hlimit = canvas.width-info.space;
    var vlimit = canvas.height-info.space;

    var fruit = {
      "pos": [],
      "num": 0,
      "limit": 1
    };

    var keyMap = {
      "left": { "code" : 65 },
      "up": { "code" : 87 },
      "right": { "code" : 68 },
      "down": { "code" : 83 }
    };
    var doNotTurn = {
      "l": "r",
      "r": "l",
      "u": "d",
      "d": "u"
    }
    var toTurn = [];

    var ctx = canvas.getContext("2d");

    function clearit(){
      ctx.save();
      ctx.setTransform(1,0,0,1,0,0);
      ctx.clearRect(0,0,canvas.width, canvas.height);
      ctx.restore();
    }

    function snakeit(){
      var i, pos;
      ctx.fillStyle = "#ffffff";
      pos = snake.body[0];

      //eat!!!!
      var eatit = false;
      for(i=0;i<fruit.num;i++){
        // console.log(fruit.pos, snake.body[0]);
        if(fruit.pos[i][0]==pos[0] && fruit.pos[i][1]==pos[1]){
          fruit.pos.splice(i,1);
          fruit.num--;
          eatit = true;
          snake.body.push(
            [snake.body[snake.len-1][0],
            snake.body[snake.len-1][1]]);
          // console.log(snake.body);
        }
      }
      for(i=snake.len-1;i>0;i--){
        snake.body[i][0] = snake.body[i-1][0];
        snake.body[i][1] = snake.body[i-1][1];
      }
      if(eatit){
        snake.len++;
        eatit = false;
      }
      if(toTurn.length>0){
        td = toTurn.shift();
        snake.direction = (td!=doNotTurn[snake.direction])?td:
          (toTurn.length>0)?toTurn.shift():snake.direction;
      }
      if(snake.direction=="r")
        pos[0]++;
      else if(snake.direction=="l")
        pos[0]--;
      else if(snake.direction=="u")
        pos[1]--;
      else if(snake.direction=="d")
        pos[1]++;
      if(pos[0]<0)
        pos[0]=hlimit/info.space;
      else if(pos[0]*info.space>hlimit)
        pos[0]=0;
      if(pos[1]<0)
        pos[1]=vlimit/info.space;
      else if(pos[1]*info.space>vlimit)
        pos[1]=0;
      for(i=0;i<snake.len;i++){
        ctx.fillRect(snake.body[i][0] * info.space, snake.body[i][1] * info.space, info.size, info.size);
        ctx.fill();
      }
    }

    function fruitit(){
      if(fruit.num<fruit.limit){
        var newx, newy;
        newx = Math.floor((Math.random()*hlimit/info.space));
        newy = Math.floor((Math.random()*vlimit/info.space));
        fruit.pos.push([newx, newy]);
        fruit.num++;
        // console.log(fruit.pos);
      }
      var i;
      ctx.fillStyle = "#ff0000";
      for(i=0;i<fruit.num;i++){
        ctx.fillRect(fruit.pos[i][0]*info.space, fruit.pos[i][1]*info.space, info.size, info.size);
        ctx.fill();
      }
    }

    window.addEventListener("keydown", function(e) {
      switch(e.keyCode){
        case 65:
        case 37:
          if(toTurn[toTurn.length-1]!="l")
            toTurn.push("l");
          break;
        case 68:
        case 39:
          if(toTurn[toTurn.length-1]!="r")
            toTurn.push("r");
          break;
        case 87:
        case 38:
          if(toTurn[toTurn.length-1]!="u")
            toTurn.push("u");
          break;
        case 83:
        case 40:
          if(toTurn[toTurn.length-1]!="d")
            toTurn.push("d");
          break;
        // default:
        //  console.log(e.keyCode);
      }
    }, false);

    function checkbody(){
      var i;
      var head = snake.body[0];
      for(i=1;i<snake.len;i++){
        if(snake.body[i][0]==head[0] && snake.body[i][1]==head[1])
          // console.log(snake.body);
          return true;
      }
      return false;
    }

    var interval = 200;
    var next = [8, 12, 20, 30];
    var go = true;
    var cycle = setInterval("cycleit()", interval);

    function cycleit(){
      clearit();
      snakeit();
      fruitit();
      if(go){
        if(snake.len==next[0]){
          interval -= 35;
          cycle = window.clearInterval(cycle);
          cycle = setInterval("cycleit()", interval);
          // console.log("accelerated");
          go = false;
        }
      }
      if(!go && next.length>0 && snake.len>next[0]){
        go = true;
        next.shift();
        // console.log("next aim is "+next[0]);
      }
      if(snake.len==40){
        alert("Enough!\n通关啦, 别玩啦!");
        cycle = window.clearInterval(cycle);
      }
      if(checkbody()==true){
        cycle = window.clearInterval(cycle);
        alert("Too bad, Hit yourself.\n撞到自己了>_<");
      }
      // console.log(snake.body[0]);
      // console.log("did it");
    }

    // alert(snake.body.length);
