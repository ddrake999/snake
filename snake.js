var Fruit, Snake, board, canv, clearit, ctx, cycle, cycleit, go, interval, level, pixel;

canv = document.getElementById("main");

canv.width = "600";

canv.height = "400";

canv.background = "#000";

ctx = canv.getContext("2d");

pixel = {
  "size": 20,
  "visible": 17
};

clearit = function() {
  ctx.save();
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  ctx.clearRect(0, 0, canv.width, canv.height);
  return ctx.restore();
};

Fruit = (function() {

  function Fruit() {
    var getPos, inSnake;
    getPos = function() {
      return [Math.floor(Math.random() * board.hlimit / pixel.size), Math.floor(Math.random() * board.vlimit / pixel.size)];
    };
    inSnake = function(pos) {
      var i, s, _i, _ref;
      s = board.snake;
      for (i = _i = 0, _ref = s.len; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
        if (pos[0] === s.body[i][0] && pos[1] === s.body[i][1]) {
          return true;
        }
      }
      return false;
    };
    this.pos = getPos();
    while (inSnake(this.pos)) {
      this.pos = getPos();
    }
  }

  Fruit.prototype.render = function() {
    ctx.fillStyle = "#ff0000";
    ctx.fillRect(this.pos[0] * pixel.size, this.pos[1] * pixel.size, pixel.visible, pixel.visible);
    return ctx.fill();
  };

  return Fruit;

})();

Snake = (function() {

  function Snake() {
    this.direction = "r";
    this.body = [[3, 0], [2, 0], [1, 0], [0, 0]];
    this.len = 4;
    this.doNotTurn = {
      "l": "r",
      "r": "l",
      "u": "d",
      "d": "u"
    };
    this.toTurn = [];
  }

  Snake.prototype.snakeit = function() {
    var ate;
    this.pos = this.body[0];
    ate = this.tryEat();
    this.advanceTail(ate);
    if (this.toTurn.length > 0) {
      this.turn();
    }
    this.advanceHead();
    return this.render();
  };

  Snake.prototype.tryEat = function() {
    var ate, i, _i, _ref;
    ate = false;
    for (i = _i = 0, _ref = board.fruit.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
      if (board.fruit[i].pos[0] === this.pos[0] && board.fruit[i].pos[1] === this.pos[1]) {
        board.fruit.splice(i, 1);
        board.fruitNum--;
        ate = true;
        this.body.push([this.body[this.len - 1][0], this.body[this.len - 1][1]]);
        break;
      }
    }
    return ate;
  };

  Snake.prototype.advanceTail = function(ate) {
    var i, _i, _ref;
    for (i = _i = _ref = this.len - 1; _ref <= 0 ? _i < 0 : _i > 0; i = _ref <= 0 ? ++_i : --_i) {
      this.body[i][0] = this.body[i - 1][0];
      this.body[i][1] = this.body[i - 1][1];
    }
    if (ate) {
      return this.len++;
    }
  };

  Snake.prototype.turn = function() {
    var td;
    td = this.toTurn.shift();
    while (td === this.doNotTurn[this.direction]) {
      td = this.toTurn.shift();
    }
    if (td) {
      return this.direction = td;
    }
  };

  Snake.prototype.advanceHead = function() {
    switch (this.direction) {
      case "r":
        this.pos[0]++;
        break;
      case "l":
        this.pos[0]--;
        break;
      case "u":
        this.pos[1]--;
        break;
      case "d":
        this.pos[1]++;
    }
    if (this.pos[0] < 0) {
      this.pos[0] = board.hlimit / pixel.size;
    } else if (this.pos[0] * pixel.size > board.hlimit) {
      this.pos[0] = 0;
    }
    if (this.pos[1] < 0) {
      return this.pos[1] = board.vlimit / pixel.size;
    } else if (this.pos[1] * pixel.size > board.vlimit) {
      return this.pos[1] = 0;
    }
  };

  Snake.prototype.render = function() {
    var i, _i, _ref, _results;
    ctx.fillStyle = "#ffffff";
    _results = [];
    for (i = _i = 0, _ref = this.len; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
      ctx.fillRect(this.body[i][0] * pixel.size, this.body[i][1] * pixel.size, pixel.visible, pixel.visible);
      _results.push(ctx.fill());
    }
    return _results;
  };

  Snake.prototype.checkbody = function() {
    var head, i, _i, _ref;
    head = this.body[0];
    for (i = _i = 1, _ref = this.len; 1 <= _ref ? _i < _ref : _i > _ref; i = 1 <= _ref ? ++_i : --_i) {
      if (this.body[i][0] === head[0] && this.body[i][1] === head[1]) {
        return true;
      }
    }
    return false;
  };

  return Snake;

})();

board = {
  init: function() {
    this.hlimit = canv.width - pixel.size;
    this.vlimit = canv.height - pixel.size;
    this.snake = new Snake;
    this.fruit = [];
    this.fruitLimit = 2;
    this.fruit.push(new Fruit);
    return this.fruitNum = 1;
  },
  addFruit: function() {
    if (this.fruitNum < this.fruitLimit) {
      this.fruit.push(new Fruit);
      this.fruitNum++;
    }
    return this.renderFruit();
  },
  renderFruit: function() {
    var f, _i, _len, _ref, _results;
    _ref = this.fruit;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      f = _ref[_i];
      _results.push(f.render());
    }
    return _results;
  }
};

board.init();

window.addEventListener("keydown", function(e) {
  var tt;
  tt = board.snake.toTurn;
  switch (e.keyCode) {
    case 65:
    case 37:
      if (tt[tt.length - 1] !== "l") {
        tt.push("l");
      }
      break;
    case 68:
    case 39:
      if (tt[tt.length - 1] !== "r") {
        tt.push("r");
      }
      break;
    case 87:
    case 38:
      if (tt[tt.length - 1] !== "u") {
        tt.push("u");
      }
      break;
    case 83:
    case 40:
      if (tt[tt.length - 1] !== "d") {
        tt.push("d");
      }
      break;
  }
  return false;
});

interval = 200;

level = [8, 12, 20, 30];

go = true;

cycle = setInterval("cycleit()", interval);

cycleit = function() {
  clearit();
  board.snake.snakeit();
  board.addFruit();
  if (go) {
    if (board.snake.len === level[0]) {
      interval -= 35;
      window.clearInterval(cycle);
      cycle = setInterval("cycleit()", interval);
      go = false;
    }
  }
  if (!go && level.length > 0 && board.snake.len > level[0]) {
    go = true;
    level.shift();
  }
  if (board.snake.len === 40) {
    window.clearInterval(cycle);
    alert("Yay!");
  }
  if (board.snake.checkbody() === true) {
    window.clearInterval(cycle);
    return alert("Too bad, Hit yourself.");
  }
};
