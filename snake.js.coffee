canv = document.getElementById("main")
canv.width = "600"
canv.height = "400"
canv.background = "#000"

ctx = canv.getContext("2d")

pixel = 
  "size": 20
  "visible": 17

clearit = ()->
  ctx.save()
  ctx.setTransform 1, 0, 0, 1, 0, 0
  ctx.clearRect 0, 0, canv.width, canv.height
  ctx.restore()

# Fruit class
class Fruit
  constructor: ()->
    getPos = ()->
      [Math.floor(Math.random()*board.hlimit/pixel.size), 
       Math.floor(Math.random()*board.vlimit/pixel.size)]

    inSnake = (pos)->
      s = board.snake
      for i in [0...s.len]
        return true if pos[0] == s.body[i][0] and pos[1] == s.body[i][1]
      return false

    @pos = getPos()
    @pos = getPos() while inSnake @pos
    # console.log @pos

  render: ()->
    ctx.fillStyle = "#ff0000"
    ctx.fillRect @pos[0]*pixel.size, @pos[1]*pixel.size, pixel.visible, pixel.visible
    ctx.fill()



# Snake class
class Snake
  constructor: ()->
    @direction = "r"
    @body = [[3,0],[2,0],[1,0],[0,0]]
    @len = 4
    @doNotTurn =
      "l": "r"
      "r": "l"
      "u": "d"
      "d": "u"
    @toTurn = []

  snakeit: ()->
    @pos = @body[0]
    ate = @tryEat()
    @advanceTail(ate)
    @turn() if @toTurn.length > 0
    @advanceHead()
    @render()
      
  tryEat: ()->
    ate = false
    for i in [0...board.fruit.length]
      # console.log fruit.pos, snake.body[0]
      if board.fruit[i].pos[0]==@pos[0] and board.fruit[i].pos[1]==@pos[1]
        board.fruit.splice(i,1)
        board.fruitNum--
        ate = true
        @body.push [@body[@len-1][0], @body[@len-1][1]]
        break
        # console.log snake.body
    return ate

  advanceTail: (ate)->
    for i in [@len-1...0]
      @body[i][0] = @body[i-1][0]
      @body[i][1] = @body[i-1][1]
    @len++ if ate

  turn: ()->
    td = @toTurn.shift()
    td = @toTurn.shift() while td == @doNotTurn[@direction] 
    @direction = td if td

  advanceHead: ()->
    switch @direction
      when "r" then @pos[0]++
      when "l" then @pos[0]--
      when "u" then @pos[1]--
      when "d" then @pos[1]++

    if @pos[0] < 0 
      @pos[0] = board.hlimit/pixel.size
    else if @pos[0]*pixel.size > board.hlimit
      @pos[0]=0

    if @pos[1] < 0
      @pos[1] = board.vlimit/pixel.size
    else if @pos[1]*pixel.size > board.vlimit
      @pos[1] = 0

  render: ()->
    ctx.fillStyle = "#ffffff"
    for i in [0...@len]
      ctx.fillRect @body[i][0] * pixel.size, @body[i][1] * pixel.size, pixel.visible, pixel.visible
      ctx.fill()

  checkbody: ()->
    head = @body[0]
    for i in [1...@len]
      if @body[i][0]==head[0] and @body[i][1]==head[1]
        # console.log snake.body
        return true
    return false


board = 
  init: ()->
    @hlimit = canv.width - pixel.size
    @vlimit = canv.height - pixel.size
    @snake = new Snake
    @fruit = []
    @fruitLimit = 2
    @fruit.push new Fruit
    @fruitNum = 1

  addFruit: ()->
    if @fruitNum < @fruitLimit
      @fruit.push new Fruit
      @fruitNum++
    @renderFruit()

  renderFruit: ()->
    for f in @fruit
      f.render()


board.init()

window.addEventListener "keydown", (e)->
  tt = board.snake.toTurn
  switch e.keyCode
    when 65, 37
      tt.push("l") unless tt[tt.length-1] == "l"      
    when 68, 39
      tt.push("r") unless tt[tt.length-1] == "r"
    when 87, 38
      tt.push("u") unless tt[tt.length-1] == "u"
    when 83, 40
      tt.push("d") unless tt[tt.length-1] == "d"
    else
    #  console.log e.keyCode
  false


interval = 200
level = [8, 12, 20, 30]
go = true
cycle = setInterval "cycleit()", interval

cycleit = ()->
  clearit()
  board.snake.snakeit()
  board.addFruit()
  if go
    if board.snake.len==level[0]
      interval -= 35
      window.clearInterval cycle
      cycle = setInterval "cycleit()", interval
      # console.log "accelerated"
      go = false;

  if !go and level.length>0 and board.snake.len>level[0]
    go = true
    level.shift()
    # console.log "next goal is "+level[0]

  if board.snake.len==40
    window.clearInterval cycle
    alert "Yay!"

  if board.snake.checkbody() == true
    window.clearInterval cycle
    alert "Too bad, Hit yourself."

  # console.log snake.body[0]
  # console.log "did it"

